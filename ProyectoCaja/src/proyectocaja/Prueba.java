package proyectocaja;

public class Prueba {

    public static void main(String[] args) {
        int ancho = 3;
        int alto = 2;
        int profundo = 6;
        Caja objeto = new Caja(ancho, alto, profundo);
        System.out.println("El volumen de la caja es: " + objeto.calcularVolumen());
    }
}
